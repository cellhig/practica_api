<?php

class Movie{

    private $conn;
    private $table_name = "movies";

    public $id;
    public $title;
    public $synopsis;
    public $date;

    public function __construct($db){

        $this->conn = $db;
    }

    function getAll(){

        $query = "SELECT * FROM ".$this->table_name." ORDER BY DESC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    function create(){

        $query = "INSERT INTO ".$this->table_name." VALUES null, title = :title, synopsis = :synopsis, date = :date";
        $stmt = $this->conn->prepare($query);

        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->title = htmlspecialchars(strip_tags($this->synopsis));
        $this->title = htmlspecialchars(strip_tags($this->date));

        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":synopsis", $this->synopsis);
        $stmt->bindParam(":date", $this->date);

        if ($stmt->execute()) {

            return true;

        } else {

            return false;
        }
    }

    function readOne(){
        
        $query = "SELECT * FROM ".$this->table_name." WHERE id = :id LIMIT 1";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":id", $this->id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->id = $row['id'];
        $this->title = $row['title'];
        $this->synopsis = $row['synopsis'];
        $this->date = $row['date'];
    }

    function update(){

        $query = "UPDATE ".$this->table_name." SET title = :title, synopsis = :synopsis, date = :date WHERE id = :id";
        $stmt = $this->conn->prepare($query);

        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->title = htmlspecialchars(strip_tags($this->synopsis));
        $this->title = htmlspecialchars(strip_tags($this->date));
        $this->id = htmlspecialchars(strip_tags($this->id));

        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":synopsis", $this->synopsis);
        $stmt->bindParam(":date", $this->date);
        $stmt->bindParam(":id", $this->id);

        if ($stmt->execute()) {
            
            return true;

        } else {

            return false;
        }
        
    }

    function delete(){

        $query = "DELETE * FROM ".$this->table_name." WHERE id = ?";
        $stmt = $this->conn->prepare($query);
        
        $this->id = htmlspecialchars(strip_tags($this->id));

        $stmt->bindParam(1, $this->id);

        if ($stmt->execute()) {
            
            return true;

        } else {
            
            return false;
        }
        
    }
}