<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/database.php';
include_once '../models/movies.php';

$database = new Database();
$db = $database->getConnection();

$movie = new Movie($db);

$stmt = $movie->getAll();
$num = $stmt->rowCount();

if ($num  > 0) {

    $movies_array = array();
    $movies_array["records"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

        extract($row);

        $movie_item = array(
            "id" => $id,
            "title" => $title,
            "synopsis" => $synopsis,
            "date" => $date
        );

        array_push($movies_array["records"], $movie_item);
    }
    http_response_code(200);
    echo json_encode($movies_array);

} else {
    
    http_response_code(400);
    echo json_encode(array(
        "code" => "400",
        "message" => "No movies found"
    ));
}