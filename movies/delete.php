<?php

header("Access-Control-Allow-Origin: *");
header("Conten-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../models/movies.php';

$database = new Database();
$db = $database->getConnection();

$movie = new Movie($db);

$data = json_decode(file_get_contents("php://input"));

$movie->id = $data->id;

if ($movie->delete()) {

    http_response_code(200);
    echo json_encode(array(
        "code" => "200",
        "message" => "Movie was deleted"
    ));

} else {

    http_response_code(503);
    echo json_encode(array(
        "code" => "503",
        "message" => "Unable to delete movie"
    ));
}