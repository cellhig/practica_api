<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: appliucation/json");

include_once '../config/database.php';
include_once '../models/movies.php';

$database = new Database();
$db = $database->getConnection();

$movie = new Movie($db);

$movie->id = isset($_GET['id']) ? $_GET['id'] : die();

$movie->readOne();

if ($movie->title != null) {

    $movie = array(
        "id" => $movie->id,
        "title" => $movie->title,
        "synopsis" => $movie->title,
        "date" => $movie->date
    );

    http_response_code(200);
    echo json_encode(array(
        "code" => "200",
        "movie" => $movie
    ));

} else {
    
    http_response_code(400);
    echo json_encode(array(
        "code" => "400",
        "message" => "Movie does not exist."
    ));
}
