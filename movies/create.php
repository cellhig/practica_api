<?php

header("Access-Control-Allow-Origin: *");
header("Conten-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../models/movies.php';

$database = new Database();
$db = $database->getConnection();

$movie = new Movie($db);

$data = json_decode(file_get_contents("php://input"));

if (!empty($data->title) && !empty($data->synopsis) && !empty($data->date)) {
    
    $movie->title = $data->title;
    $movie->synopsis = $data->synopsis;
    $movie->date = $data->date;

    if ($movie->create()) {
        
        http_response_code(201);
        echo json_encode(array(
            "code" => "201",
            "message" => "Movie was created."

        ));

    } else {
        http_response_code(503);
        echo json_encode(array(
            "code" => "503",
            "message" => "Unable to create movie"
        ));
    }
    
} else {

    http_response_code(400);
    echo json_encode(array(
        "code" => "400",
        "message" => "Unable to create movie. Data is incomplete"
    ));

}